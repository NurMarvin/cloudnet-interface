const socket = require("../../../main/script/socket");

let consoleServerName, serverConsole, commandInput;

function cache() {
    if(!consoleServerName) {
        consoleServerName = document.getElementById("consoleServerName");
        serverConsole = document.getElementById("serverConsole");
        commandInput = document.getElementById("commandInput");
    }
}

const messageListener = (queryResult) => {
    const message = queryResult["message"];
    const content = queryResult["content"];
    if(message && content) {
        console.log(message + content);
        if(message === "consoleLine")
            serverConsole.innerText += "\n" + content;
    }
};

function start(server) {
    consoleServerName.innerText = server;
    socket.registerListener("message", messageListener);
    socket.sendSocketMessage("startconsolesession", server);
}

function stop() {
    socket.sendSocketMessage("stopconsolesession");
    socket.removeListener("message", messageListener);
}

module.exports.cache = cache;
module.exports.start = start;
module.exports.stop = stop;