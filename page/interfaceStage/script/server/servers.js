const SectionUpdater = require("./sectionUpdater"), sectionUpdater = new SectionUpdater();
const socket = require("../../../../main/script/socket");
const pages = require("../../../../main/script/pages");
const serverConsole = require("../../console/console");
let cloudNet;

(async () => {
    cloudNet = require("../../../../main/script/cloudnet").getCloudNet();

    sectionUpdater.cacheDOM(document.getElementById("servers"));
    document.getElementById("refresher").addEventListener("click", updateServersPage, false);

    await updateServersPage();
})();

async function updateServersPage() {
    sectionUpdater.updateSection(cloudNet.serverGroups, "groupSection", updateGroupElement);
    sectionUpdater.updateSection(cloudNet.serverInfos, "serverSection", updateServerElement);
}

function updateGroupElement(collectionElement, container, element, elementName) {
    element.querySelector("#startButton").addEventListener("click",
        () => socket.sendSocketMessage("startserver", elementName));
}

function updateServerElement(collectionElement, container, element, elementName) {
    element.querySelector("#stopButton").addEventListener("click",
        () => socket.sendSocketMessage("stopserver", elementName));
    element.querySelector("#consoleButton").addEventListener("click", () => {
        serverConsole.start(elementName);
        pages.showPage("console");
    });
}

