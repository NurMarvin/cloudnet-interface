require("materialize-css");
class SectionUpdater {

    /**
     * Updates a section
     *
     * @param collection collection where the elements should be taken from
     * @param section the section of the elements
     * @param elementUpdate function which updates the element-clone
     */

    updateSection(collection, section, elementUpdate) {
        let collapsible, info, template;
        if(section === "groupSection") {
            collapsible = this.groupCollapsible;
            info = this.noGroupInfo;
            template = this.groupTemplate;
        } else {
            collapsible = this.serverCollapsible;
            info = this.noServerInfo;
            template = this.serverTemplate;
        }

        collapsible.innerHTML = "";

        // Showing or hiding the info
        if(!Object.keys(collection).length) {
            if(info.classList.contains("page"))
                info.classList.remove("page");
            return;
        } else if(!info.classList.contains("page")) {
            info.classList.add("page");
        }

        for(const name in collection) {
            if(collection.hasOwnProperty(name)) {
                const li = template.content.cloneNode(true);
                li.querySelector(".collapsible-header").innerText = name;
                elementUpdate(collection[name], collapsible, li, name);
                collapsible.appendChild(li);
            }
        }
    };

    cacheDOM(page) {
        // Collapsible initialization

        const groupSection = page.querySelector("#groupSection");
        const serverSection = page.querySelector("#serverSection");

        this.groupCollapsible = groupSection.querySelector("ul");
        this.serverCollapsible = serverSection.querySelector("ul");
        M.Collapsible.init(this.groupCollapsible, {});
        M.Collapsible.init(this.serverCollapsible, {});

        // Modal initialization

        M.Modal.init(document.querySelectorAll('.modal'), {});


        // Caching elements

        this.noGroupInfo = page.querySelector("#noGroupInfo");
        this.noServerInfo = page.querySelector("#noServerInfo");

        this.groupTemplate = page.querySelector("#groupTemplate");
        this.serverTemplate = page.querySelector("#serverTemplate");
    };
}

module.exports = SectionUpdater;