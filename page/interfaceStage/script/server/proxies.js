const SectionUpdater = require("./sectionUpdater"), sectionUpdater = new SectionUpdater();
const socket = require("../../../../main/script/socket");
const pages = require("../../../../main/script/pages");
const serverConsole = require("../../console/console");
let cloudNet;

(async () => {
    cloudNet = require("../../../../main/script/cloudnet").getCloudNet();

    sectionUpdater.cacheDOM(document.getElementById("proxies"));
    document.getElementById("refresher").addEventListener("click", updateProxiesPage, false);

    await updateProxiesPage();
})();

async function updateProxiesPage() {
    sectionUpdater.updateSection(cloudNet.proxyGroups, "groupSection", updateGroupElement);
    sectionUpdater.updateSection(cloudNet.proxyInfos, "serverSection", updateServerElement);
}


function updateGroupElement(collectionElement, container, element, elementName) {
    element.querySelector("#startButton").addEventListener("click",
        () =>  socket.sendSocketMessage("startproxy", elementName));
}

function updateServerElement(collectionElement, container, element, elementName) {
    element.querySelector("#stopButton").addEventListener("click",
        () => socket.sendSocketMessage("stopproxy", elementName));
    element.querySelector("#consoleButton").addEventListener("click", () => {
        serverConsole.start(elementName);
        pages.showPage("console");
    });

}