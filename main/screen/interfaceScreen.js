require("materialize-css");
const cloudNetModule = require("../script/cloudnet");
const pages = require("../script/pages");
const socket = require("../script/socket");
let refresher, aElement, startText, userName, userUUID;

const messageListener = (message) => {
    if(message["message"] && message["content"]) {
        if (message["message"] === "notification")
            M.toast({html: message["content"]});
    }
};
socket.registerListener("message", messageListener);
socket.registerListener("close", (closeEvent) => {
    socket.removeListener("message", messageListener);

    pages.showPage("login");

    M.toast({html: "Die Verbindung mit der Cloud wurde getrennt"});
    console.log("Connection closed.");
    console.log(closeEvent);
});


async function init() {
    // Closing the sidebar when clicking on an item
    M.Sidenav.init(document.querySelectorAll(".sidenav"), {});
    const as = document.querySelectorAll("a");
    Array.prototype.forEach.call(as, (a) => {
        if(a.parentElement.parentElement.classList.contains("sidenav"))
            a.addEventListener("click", () => M.Sidenav.getInstance(document.querySelector(".sidenav")).close());
    });

    document.getElementById("menu").classList.remove("page");

    // Caching the elements
    refresher = document.getElementById("refresher");
    refresher.classList.remove("page");

    aElement = refresher.querySelector("a");
    startText = aElement.innerHTML;

    userName = document.getElementById("userName");
    userUUID = document.getElementById("userUUID");

    refresher.addEventListener("click", updateSideNav, true);

    await updateSideNav();
}

async function updateSideNav() {
    aElement.innerHTML = startText.replace("Aktualisieren", "Aktualisiert ...");

    const cloudNet = cloudNetModule.getCloudNet();

    await cloudNet.update();
    console.log(cloudNet);

    userName.innerText =  cloudNet.userInfo["name"];
    userUUID.innerText = cloudNet.userInfo["uuid"];

    aElement.innerHTML = startText;
}

module.exports.init = init;

