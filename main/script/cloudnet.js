require("materialize-css");
const cloudNetKey = Symbol.for("cloudNet.interface.cloudNet");
const socket = require("./socket");

class CloudNet {


    /**
     * Updates the properties of this object from cloudNet
     *
     * @returns {Promise<void>}
     */

    async update() {
        console.log("Updating attributes ...");

        this.serverInfos = await socket.sendSocketQuery("serverinfos");
        this.proxyInfos = await socket.sendSocketQuery("proxyinfos");

        this.userInfo = await socket.sendSocketQuery("userinfo");

        this.statistics = await socket.sendSocketQuery("statistics");

        const jsonMemory = await socket.sendSocketQuery("memory");
        this.maxMemory = jsonMemory["maxMemory"];
        this.usedMemory = jsonMemory["usedMemory"];

        const cpuJson = await socket.sendSocketQuery("cpuusage");
        this.cpuUsage = cpuJson < 0 ? 0 : cpuJson.toFixed(2);

        const networkJson = await socket.sendSocketQuery("cloudnetwork");
        this.registeredPlayers = networkJson["registeredPlayerCount"] || 0;
        this.onlineCount = networkJson["onlineCount"] || 0;
        this.serverGroups = networkJson["serverGroups"] || [];
        this.proxyGroups = networkJson["proxyGroups"] || [];
        this.wrappers = networkJson["wrappers"] || [];
    }

}

module.exports.getCloudNet = () => {
    return global[cloudNetKey];
};

module.exports.setCloudNet = (newCloudNet) => {
    global[cloudNetKey] = newCloudNet;
};

module.exports.CloudNet = CloudNet;

