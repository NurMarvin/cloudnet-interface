const serverConsole = require("../../page/interfaceStage/console/console");
let links;

document.addEventListener("DOMContentLoaded", () =>
    links = document.querySelectorAll('link[rel="import"]'));

/*
 * Showing a page when an element has the page-data and has been clicked
 */

document.addEventListener("click", (event) => {
    if(event.target.dataset["page"])
        showPage(event.target.dataset["page"])

});

/**
 * Loads the content of the pages in the document
 */

function loadStagePages(stage) {
    Array.prototype.forEach.call(links, (link) => {
        if(link.dataset["stage"] === stage) {
            const div = link.import.querySelector(".page");
            const main = document.querySelector("main");
            main.appendChild(div);
        }
    });
    if(stage === "interface")
        serverConsole.cache();
}

/**
 * @returns {boolean} if the pages of the interface-Stage are loaded
 */

function areInterfacePagesLoaded() {
    return document.getElementById("home") !== null;
}

/**
 * Shows a page
 *
 * @param id id of the page-Div
 */

function isShown(id) {
    const page = document.getElementById(id);
    return page && page.classList.contains("show");
}

function showPage(id) {
    if(!isShown(id)) {

        if(isShown("console"))
            serverConsole.stop();

        hidePages();
        const page = document.getElementById(id);
        if (page && page.classList.contains("page"))
            page.classList.add("show");
    }
}

function hidePage(id) {
    if(isShown(id)) {
        const page = document.getElementById(id);
        if (page && page.classList.contains("page") && page.classList.contains("show"))
            page.classList.remove("show");
    }
}


function hidePages() {
    const pages = document.querySelectorAll(".page.show");
    Array.prototype.forEach.call(pages, (page) => page.classList.remove("show"));
}

async function loadInterfacePageScripts() {
    await require("../screen/interfaceScreen").init();
    require("../../page/interfaceStage/script/home");
    require("../../page/interfaceStage/script/server/proxies");
    require("../../page/interfaceStage/script/server/servers");
}

module.exports.showPage = showPage;
module.exports.hidePage = hidePage;
module.exports.hidePages = hidePages;
module.exports.loadStagePages = loadStagePages;
module.exports.areInterfacePagesLoaded = areInterfacePagesLoaded;
module.exports.loadInterfacePageScripts = loadInterfacePageScripts;

