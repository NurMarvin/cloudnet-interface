const webSocketKey = Symbol.for("cloudNet.interface.webSocket");
const listenerKey = Symbol.for("cloudNet.interface.listener");

let webSocket, listeners = [];

function hasKey(key) {
    return Object.getOwnPropertySymbols(global).indexOf(key) > -1;
}

if(hasKey(webSocketKey))
    webSocket = global[webSocketKey];

if(hasKey(listenerKey))
    listeners = global[listenerKey];


function openConnection(url) {
    webSocket = new WebSocket(url);
    global[webSocketKey] = webSocket;


    registerListener("error", (errorEvent) => {
        console.log("Error on the Socket-connection");
        console.log(errorEvent);
    });

    webSocket.onmessage = (messageEvent) => listeners.forEach((listener, index) => {
        if(listener["type"] === "message") {
            const executor = listener["executor"];
            executor(JSON.parse(messageEvent["data"]));
            if(listener["queryListener"])
                listeners.splice(index, 1);
        }
    });
    webSocket.onerror = (errorEvent) => listeners.forEach((listener, index) => {
        if(listener["type"] === "error") {
            const executor = listener["executor"];
            executor(errorEvent);
            if(listener["queryListener"])
                listeners.splice(index, 1);
        }
    });
    webSocket.onclose = (closeEvent) => listeners.forEach((listener, index) => {
        if(listener["type"] === "close") {
            const executor = listener["executor"];
            executor(closeEvent);
            if(listener["queryListener"])
                listeners.splice(index, 1);
        }
    });


    return new Promise(resolve => webSocket.onopen = () => {
        console.log("Successfully connected to WebSocketServer on " + url);
        resolve();
    });

}

function registerListener(type, executor) {
    listeners.push({
        type: type,
        executor: executor,
        queryListener: false
    });
}

function removeListener(type, executor) {
    listeners.forEach((listener, index) => {
        if(listener["type"] === type
            && listener["executor"] === executor)
            listeners.splice(index, 1);
    })
}


function sendSocketQuery0(message, values) {
    return new Promise(resolve => {
        const listener = {
            type: "message",
            executor: (queryResult) => {
                if(queryResult["message"] === message)
                    resolve(queryResult["response"]);
            },
            queryListener: true
        };
        listeners.push(listener);
        sendSocketMessage0(message, values);
    });
}

function sendSocketQuery(message, ...values) {
    return sendSocketQuery0(message, values);
}

function sendSocketMessage0(message, values) {
    const jsonMessage = {
        message: message,
        values: values
    };
    webSocket.send(JSON.stringify(jsonMessage));
}

function sendSocketMessage(message, ...values) {
    sendSocketMessage0(message, values);
}


module.exports.openConnection = openConnection;
module.exports.registerListener = registerListener;
module.exports.removeListener = removeListener;
module.exports.sendSocketMessage = sendSocketMessage;
module.exports.sendSocketQuery = sendSocketQuery;
module.exports.sendSocketQuery0 = sendSocketQuery0;

module.exports.getWebSocket = () => {
    return global[webSocketKey];
};



