const {app, BrowserWindow} = require("electron");

let window;

function createWindow () {
    window = new BrowserWindow({show: false, backgroundColor: "#2C2F33", width: 1300, height: 750});
    //window.setMenu(null);

    window.loadFile("main/screen/screen.html");

    window.on("ready-to-show", () => window.show());
    window.on("closed", () => window = null);
}

app.on("ready", createWindow);

app.on("activate", () => {
    if(window === null)
        createWindow();
});

app.on("window-all-closed", () => {
    if (process.platform !== "darwin")
        app.quit();
});
