const electron = require("electron");
const fs = require("fs");
const path = require("path");

class Storage {

    constructor(directory, file) {
        this.path = path.join((electron.app || electron.remote.app).getPath(directory), file);
        this.data = {};
    }

    put(key, value) {
        this.data[key] = value;
    }

    get(key) {
        return this.data[key];
    }

    has(key) {
        return this.get(key) !== undefined;
    }

    loadData() {
        try {
            this.data = JSON.parse(fs.readFileSync(this.path, "utf-8"));
        } catch (exception) {
            this.saveData();
        }
    }

    saveData() {
        fs.writeFileSync(this.path, JSON.stringify(this.data));
    }

}

module.exports = Storage;